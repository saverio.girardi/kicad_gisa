EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title "USB ESD protected"
Date "2021-04-09"
Rev "0.1"
Comp "Gisa corp."
Comment1 "Saverio Girardi"
Comment2 "Design by:"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_B_Micro J?
U 1 1 6070F92A
P 4200 4250
F 0 "J?" H 4257 4717 50  0000 C CNN
F 1 "USB_B_Micro" H 4257 4626 50  0000 C CNN
F 2 "" H 4350 4200 50  0001 C CNN
F 3 "~" H 4350 4200 50  0001 C CNN
	1    4200 4250
	1    0    0    -1  
$EndComp
$Comp
L Power_Protection:USBLC6-2SC6 U?
U 1 1 607103D0
P 6750 4250
F 0 "U?" H 6500 4600 50  0000 C CNN
F 1 "USBLC6-2SC6" H 7100 4600 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 6000 4650 50  0001 C CNN
F 3 "http://www2.st.com/resource/en/datasheet/CD00050750.pdf" H 6950 4600 50  0001 C CNN
	1    6750 4250
	1    0    0    -1  
$EndComp
Text Notes 6800 4650 0    50   ~ 0
 C7519
$Comp
L power:+5V #PWR?
U 1 1 60711742
P 4550 4000
F 0 "#PWR?" H 4550 3850 50  0001 C CNN
F 1 "+5V" H 4565 4173 50  0000 C CNN
F 2 "" H 4550 4000 50  0001 C CNN
F 3 "" H 4550 4000 50  0001 C CNN
	1    4550 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4050 4550 4050
Wire Wire Line
	4550 4050 4550 4000
$Comp
L power:GND #PWR?
U 1 1 60711F49
P 4200 4700
F 0 "#PWR?" H 4200 4450 50  0001 C CNN
F 1 "GND" H 4200 4527 50  0000 C CNN
F 2 "" H 4200 4700 50  0001 C CNN
F 3 "" H 4200 4700 50  0001 C CNN
	1    4200 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 4650 4200 4700
$Comp
L power:GND #PWR?
U 1 1 607125BA
P 6750 4750
F 0 "#PWR?" H 6750 4500 50  0001 C CNN
F 1 "GND" H 6750 4577 50  0000 C CNN
F 2 "" H 6750 4750 50  0001 C CNN
F 3 "" H 6750 4750 50  0001 C CNN
	1    6750 4750
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 60712D2F
P 6650 7400
F 0 "#PWR?" H 6650 7250 50  0001 C CNN
F 1 "+5V" H 6665 7573 50  0000 C CNN
F 2 "" H 6650 7400 50  0001 C CNN
F 3 "" H 6650 7400 50  0001 C CNN
	1    6650 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60713436
P 6150 7350
F 0 "#PWR?" H 6150 7100 50  0001 C CNN
F 1 "GND" H 6150 7177 50  0000 C CNN
F 2 "" H 6150 7350 50  0001 C CNN
F 3 "" H 6150 7350 50  0001 C CNN
	1    6150 7350
	1    0    0    -1  
$EndComp
Text HLabel 6600 7500 0    50   Input ~ 0
+5V
Wire Wire Line
	6650 7400 6650 7500
Text HLabel 6100 7300 0    50   Input ~ 0
GND
Wire Wire Line
	6100 7300 6150 7300
Wire Wire Line
	6150 7300 6150 7350
Wire Wire Line
	6600 7500 6650 7500
Text GLabel 4500 4250 2    50   Input ~ 0
USB_CONN_D+
Text GLabel 4500 4350 2    50   Input ~ 0
USB_CONN_D-
NoConn ~ 4500 4450
NoConn ~ 4100 4650
$Comp
L power:+5V #PWR?
U 1 1 60715784
P 6750 3750
F 0 "#PWR?" H 6750 3600 50  0001 C CNN
F 1 "+5V" H 6765 3923 50  0000 C CNN
F 2 "" H 6750 3750 50  0001 C CNN
F 3 "" H 6750 3750 50  0001 C CNN
	1    6750 3750
	1    0    0    -1  
$EndComp
Text GLabel 7250 4350 2    50   Input ~ 0
USB_CONN_D+
Text GLabel 6250 4350 0    50   Input ~ 0
USB_CONN_D-
Text HLabel 7250 4150 2    50   Input ~ 0
USB_D+
Text HLabel 6250 4150 0    50   Input ~ 0
USB_D-
$EndSCHEMATC
